import axios from "axios";
import { api } from "../endpoint";

class contactService {
    getContactAll() {
      return axios.get(api.contact)
    }

    getContactById(id) {
        return axios.get(api.contact+'/'+id)
    }

    postContact(data) {
        return axios.post(api.contact, data)
    }

    putContact(id, data) {
        return axios.put(api.contact+'/'+id, data)
    }

    deleteContact(id) {
        return axios.delete(api.contact+'/'+id)
    }
}

export default new contactService();
