import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  View,
  Image, TouchableOpacity, TouchableWithoutFeedback, ToastAndroid, Alert, RefreshControl
} from 'react-native';
import {
  ApplicationProvider,
  ListItem,
  Avatar,
  IconRegistry,
  Icon,
  Text,
  Input,
  Layout,
  Spinner,
  Button, Modal, Card
} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import contactService from './service/contact';

/**
 * Use any valid `name` property from eva icons (e.g `github`, or `heart-outline`)
 * https://akveo.github.io/eva-icons
 */

const ItemImage = props => (
  <Avatar
    size="giant"
    source={{
      uri: props.photo,
    }}
    ImageComponent={ImageBackground}
  />
);

const renderItemIcon = props => <Icon {...props} name="person" />;

const LoadingIndicator = props => (
  <View style={[props.style, styles.indicator]}>
    <Spinner size="small" />
  </View>
);

const ContactList = (props) => {
  return props.contact.map(data => {
    return (
      <ListItem
        key={data.id}
        title={evaProps => (
          <Text {...evaProps} style={styles.text}>
            {data.firstName + ' ' + data.lastName}
          </Text>
        )}
        description={evaProps => (
          <Text {...evaProps}>{'Umur ' + data.age}</Text>
        )}
        accessoryLeft={ItemImage(data)}
        onPress={() => {
          props.navigation.navigate('FormContact', {
            title : 'Update Contact',
            contactData : data
          })
        }}
      />
    );
  });
};

const HomeScreen = ({navigation}) => {
  const [contact, setContact] = useState([]);
  const [contactFilter, setContactFilter] = useState([]);
  const [refreshing, setRefreshing] = React.useState(false);
  
  useEffect(() => {
    loadContact()
  }, []);

  const loadContact = () => {
    setRefreshing(true);
    contactService
      .getContactAll()
      .then(res => {
        if (res.status === 200) {
          let contactList = res.data.data.sort(function (a, b) {
            if (a.firstName < b.firstName) {
              return -1;
            }
            if (a.firstName > b.firstName) {
              return 1;
            }
            return 0;
          });
          setContact(contactList);
          setContactFilter(contactList);
        }

        if (res.status !== 200) {
          console.log('Error Status ', res.status);
        }
      })
      .catch(e => {
        console.log(e);
      })
      .then(() => {
        setRefreshing(false)
      });
  }

  const onRefresh = React.useCallback(() => {
    loadContact()
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ListItem
        title="New Contact"
        description="Add New Contact"
        accessoryLeft={renderItemIcon}
        onPress={() => navigation.navigate('FormContact')}
      />
      <Input
        style={styles.input}
        placeholder="Find Contact"
        onChangeText={ev =>
          {
            let filter = contact.filter((data) => {
              return data.firstName.match(ev)
            })
            
            setContactFilter(filter)
          }
        }
      />
      <ScrollView 
        style={styles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      >
        <ContactList navigation={navigation} contact={contactFilter} />
      </ScrollView>
    </SafeAreaView>
  );
};


const FormContact = ({ navigation, route }) => {
  const { title, contactData } = route.params;

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: title ? title : 'Add New Contact',
    });
  }, [navigation, title]);

  const [value, setValue] = useState({
    photo : 'https://snack-web-player.s3.us-west-1.amazonaws.com/v2/42/static/media/react-native-logo.79778b9e.png'
  });
  const [loading, setLoading] = useState(false);
  const [loadingDelete, setLoadingDelete] = useState(false);

  const clearInput = (inputName) => {
    inputVal({
      name : inputName,
      value : ""
    })
  };
  const RenderIcon = (props) => (
    <TouchableWithoutFeedback onPress={() => {
      clearInput(props.inputName)
    }}>
      <Icon {...props} name={'close-circle-outline'}/>
    </TouchableWithoutFeedback>
  );
  const postContact = () => {
    setLoading(true)
    contactService
      .postContact(value)
      .then((res) => {
        console.log(res.status);
        if (res.status === 201) {
          ToastAndroid.show("New Contact Success Added", ToastAndroid.SHORT);
          setValue({
            photo : 'https://snack-web-player.s3.us-west-1.amazonaws.com/v2/42/static/media/react-native-logo.79778b9e.png'
          })
        }
        if (res.status !== 201) {
          ToastAndroid.show("Please Check Your Internet Connection!", ToastAndroid.SHORT);
        }
      })
      .catch((err) => {
        console.log('====================================');
        console.log(err);
          ToastAndroid.show("Please Check Your Internet Connection!", ToastAndroid.SHORT);
          setValue({
            photo : 'https://snack-web-player.s3.us-west-1.amazonaws.com/v2/42/static/media/react-native-logo.79778b9e.png'
          })
        console.log('====================================');
      })
      .then(() => {
        setLoading(false)
      })
  }

  const putContact = () => {
    setLoading(true)
    contactService
      .putContact(contactData.id, value)
      .then((res) => {
        console.log(res.status);
        if (res.status === 201) {
          ToastAndroid.show("Updated Contact Success", ToastAndroid.SHORT);
        }
        if (res.status !== 201) {
          ToastAndroid.show("Please Check Your Internet Connection!", ToastAndroid.SHORT);
        }
      })
      .catch((err) => {
        console.log('====================================');
        console.log(err);
          ToastAndroid.show("Please Check Your Internet Connection!", ToastAndroid.SHORT);
        console.log('====================================');
      })
      .then(() => {
        setLoading(false)
      })
  }

  const deleteContact = () => {
    Alert.alert(
      "Delete Contact "+value.firstName+' '+value.lastName,
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => Alert.alert("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: () => {
            setLoadingDelete(true);
            contactService
              .deleteContact(contactData.id)
              .then((res) => {
                console.log(res.status);
                if ([200, 202, 204]) {
                  ToastAndroid.show("Contact Deleted Success", ToastAndroid.SHORT);
                }
              })
              .catch((res, msg) => {
                console.log(res.message);
                ToastAndroid.show("Please Check Your Internet Connection", ToastAndroid.SHORT);
                navigation.navigate('Home')
              })
              .then(() => {
                setLoadingDelete(false);
              })
          },
          style: "default",
        }
      ]
    )
  }

  const inputVal = ev => {
    if (ev.value == '') {
      delete value[ev.name];
    }

    if (ev.value != '') {
      setValue({
        ...value,
        [ev.name]: ev.value,
      });
    }
  }

  useEffect(() => {
    if (contactData) {
      setValue({
        ...value,
        firstName : contactData.firstName,
        lastName : contactData.lastName,
        age : contactData.age.toString(),
        photo : contactData.photo
      })
    }
  }, [])

  return (
    <SafeAreaView style={[styles.container, styles.FormContactContainer]}>
      <Layout style={{
        alignItems: 'center'
      }}>
        <TouchableOpacity onPress={() => setVisible(true)}>
          <Image
            style={{
              width: 50,
              height: 50,
            }}
            source={{
              uri: value.photo,
            }}
          />
        </TouchableOpacity>
      </Layout>
      <Input
        style={styles.input}
        placeholder="First Name"
        defaultValue={value.firstName}
        accessoryRight={<RenderIcon inputName="firstName" />}
        onChangeText={ev =>
          inputVal({
            name: 'firstName',
            value: ev,
          })
        }
      />
      <Input
        style={styles.input}
        placeholder="Last Name"
        defaultValue={value.lastName}
        accessoryRight={<RenderIcon inputName="lastName" />}
        onChangeText={ev =>
          inputVal({
            name: 'lastName',
            value: ev,
          })
        }
      />
      <Input
        style={styles.input}
        placeholder="Age"
        defaultValue={value.age}
        accessoryRight={<RenderIcon inputName="age" />}
        onChangeText={ev =>
          inputVal({
            name: 'age',
            value: ev,
          })
        }
      />
      <Input
        style={styles.input}
        multiline={true}
        textStyle={{ minHeight: 64, maxHeight: 120 }}
        placeholder="Link Photo"
        defaultValue={value.photo}
        inputName="photo"
        accessoryRight={<RenderIcon inputName="photo" />}
        onChangeText={ev =>
          inputVal({
            name: 'photo',
            value: ev,
          })
        }
      />
      {
        contactData && contactData.id ?
        <>
        <Button
          style={{
            margin: 15,
            width: '93%',
            position: 'absolute',
            bottom: 50,
            left: 0,
          }}
          appearance={loading ? 'outline' : 'filled'}
          onPress={() => putContact() }
          accessoryLeft={loading ? LoadingIndicator : ''}>
          Update Contact
        </Button>
        <Button
          style={styles.button}
          status="danger"
          appearance={loadingDelete ? 'outline' : 'filled'}
          onPress={() => deleteContact() }
          accessoryLeft={loadingDelete ? LoadingIndicator : ''}>
          Delete Contact
        </Button>
        </> 
        : 
        <Button
          style={styles.button}
          appearance={loading ? 'outline' : 'filled'}
          onPress={() => postContact() }
          accessoryLeft={loading ? LoadingIndicator : ''}>
          Save New Contact
        </Button>
      }
    </SafeAreaView>
  );
};

const Stack = createStackNavigator();

export default () => (
  <>
    <IconRegistry icons={EvaIconsPack} />
    <ApplicationProvider {...eva} theme={eva.light}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
              title: 'Get Contact',
              headerStyle: {
                backgroundColor: '#2980b9',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen
            name="FormContact"
            component={FormContact}
            options={{
              title: 'Add New Contact',
              headerStyle: {
                backgroundColor: '#2980b9',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
            initialParams={{
              id : 0
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </ApplicationProvider>
  </>
);

const styles = StyleSheet.create({
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  button: {
    margin: 15,
    width: '93%',
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  FormContactContainer: {
    paddingTop: 15,
  },
  container: {
    minHeight: 128,
    backgroundColor: '#fff',
    flex: 1,
  },
  input: {
    marginHorizontal: 15,
    marginVertical: 5,
  },
  rowContainer: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  scrollView: {
    marginHorizontal: 0,
  },
  avatar: {
    // margin: 8,
  },
  text: {
    paddingLeft: 8,
    fontWeight: 'bold',
  },
});
